# Configuration Service

## Description

To load an application always is needed parameters of variables. Some of them can be diferents depending on the environment you want to deploy. This library takes care of it.

## Installation

This project has an automated deployment to pypi, so only is needed to use pip command:

```
>> pip install cfg-service
```

After installation you have available a package called `cfgsrv`. The project depends of library `pyyaml` if it uses yaml files to define variables.

## Usage

To be documented.


## Support

Send any suggestion to sruiz@indoorclima.com or salvador.ruiz.r@gmail.com. Any ideas or support is well recieved.

## Roadmap

- [ ] Increment versioning when pushing
- [ ] Integrate with gitlab continuous integration to publish to pypi as library
- [ ] Improve coverage rate to > 96%
- [ ] Improve usage documentation with sphinx
- [ ] Upload to readthedocs

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

We would like to thank the contribution of
For sure the ideas of Eric Evans and Domain Design Development are the base of this project to be used with python language. Without this seed it has not sensse

## License

This is under LGPL lincense. You can use and modify this library.

## Project status

It is used in projects developed currently by the company IndoorClima.
