from unittest.mock import patch, mock_open
from cfgsrv.os import OsEnvironmentConfigService, ConfigMapService
from tempfile import TemporaryDirectory
import os


class An_OsEnvironmentConfigService:
    def should_return_environ_value(self):
        os.environ["VAR"] = "value"

        config = OsEnvironmentConfigService()

        assert config["VAR"] == "value"

    def should_return_default_if_no_exist(self):
        config = OsEnvironmentConfigService()
        assert config.get("OTHER_VAR", 1) == 1

    def should_return_none_if_no_exist(self):
        config = OsEnvironmentConfigService()
        assert config.get("no-exist") is None

    def should_return_dict_if_var_with_dot(self):
        os.environ["var.key2"] = "value2"
        os.environ["var.key1"] = "value1"

        config = OsEnvironmentConfigService()

        assert config.get("var") == {"key1": "value1", "key2": "value2"}

        del os.environ["var.key1"]
        del os.environ["var.key2"]

    def should_have_nested_vars(self):
        os.environ["var.key1.key2"] = "value1"

        config = OsEnvironmentConfigService()

        assert config.get("var") == {"key1": {"key2": "value1"}}
        assert config.get("var.key1.key2") == "value1"

        del os.environ["var.key1.key2"]


class A_ConfigMapService:
    @classmethod
    def setup_class(cls):
        cls.dir = TemporaryDirectory()

    def teardown_method(self):
        for filename in os.listdir(self.dir.name):
            os.remove(os.path.join(self.dir.name, filename))

    def given_a_file_with_content(self, filename: str, content: str):
        full_filename = os.path.join(self.dir.name, filename)
        with open(full_filename, "w") as f:
            f.write(content)

    def should_return_values(self):
        service = ConfigMapService(self.dir.name)
        self.given_a_file_with_content("default.value", "value")

        assert service.get("default.value") == "value"

    def should_return_default_parameter(self):
        service = ConfigMapService(self.dir.name)

        assert service.get("variable", "default") == "default"

    def should_return_none_if_no_exist(self):
        config = ConfigMapService(self.dir.name)

        assert (
            config.get(
                "no-exist",
            )
            is None
        )

    def should_return_dict_if_var_with_dot(self):
        config = ConfigMapService(self.dir.name)
        self.given_a_file_with_content("var.key2", "value2")
        self.given_a_file_with_content("var.key1", "value1")

        assert config.get("var") == {"key1": "value1", "key2": "value2"}

    def should_have_nested_vars(self):
        config = ConfigMapService(self.dir.name)
        self.given_a_file_with_content("var.key1.key2", "value")

        assert config.get("var.key1.key2") == "value"
        assert config.get("var") == {"key1": {"key2": "value"}}
