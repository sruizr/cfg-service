from copy import deepcopy
from unittest.mock import mock_open, patch

from cfgsrv.yaml import YamlConfigService

_CONTENT = """
string_par: string-value
int_par: 1
float_par: 1.2
listPar: [one, two, 3]
dictPar:
  nested:
    key: value
"""


class A_YamlConfigService:
    def setup_method(self):
        self._open_patch = patch("builtins.open", mock_open(read_data=_CONTENT))
        self.open = self._open_patch.start()

    def teardown_method(self):
        self._open_patch.stop()

    def should_return_none_if_no_exist(self):
        config = YamlConfigService("filename.yaml")
        assert config.get("no-exist") is None

    def should_load_from_filename(self):
        YamlConfigService("filename.yaml")
        self.open.assert_called_with("filename.yaml", "r")

    def should_get_parameters_from_its_environment(self):
        config = YamlConfigService("filename.yaml")

        assert config.get("string_par") == "string-value"

    def should_return_always_string_values(self):
        config = YamlConfigService("filename.yaml")

        assert config.get("float_par") == "1.2"
        assert config.get("int_par") == "1"

    def should_get_list_parameters(self):
        config = YamlConfigService("filename.yaml")

        assert config.get("list_par") == ["one", "two", "3"]

    def should_get_dict_parameters(self):
        config = YamlConfigService("filename.yaml")

        assert config.get("dict_par") == {"nested": {"key": "value"}}

    def should_get_original_dict_parameters(self):
        config = YamlConfigService("filename.yaml")

        value = config.get("dict_par")
        original = deepcopy(value)
        value["new_key"] = 1

        assert original != value

    def should_access_to_sub_items_of_dict_par(self):
        config = YamlConfigService("filename.yaml")

        assert config.get("dict_par.nested.key") == "value"
        assert config.get("dict_par.no-exist") is None
        assert config.get("dict_par.nested") == {"key": "value"}
