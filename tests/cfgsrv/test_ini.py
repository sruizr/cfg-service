from unittest.mock import mock_open, patch
import pytest

from cfgsrv.ini import IniConfigService
from cfgsrv.core import ParameterNotFound


ini_file_content = """
[app]
key = value

[other_app]
username = username
"""


class A_IniConfigService:
    def setup_method(self):
        self._open_patch = patch("builtins.open", mock_open(read_data=ini_file_content))
        self.open = self._open_patch.start()

    def given_a_ini_config_service(self):
        return IniConfigService("filename.ini")

    def teardown_method(self):
        self._open_patch.stop()

    def should_return_config_with_simple_section_name(self):
        config = self.given_a_ini_config_service()
        assert config["app.key"] == "value"

    def should_return_credential_with_complex_section_name(self):
        config = self.given_a_ini_config_service()
        assert config["other_app.username"] == "username"

    def should_raise_parameter_not_found_if_no_exist(self):
        config = self.given_a_ini_config_service()
        with pytest.raises(ParameterNotFound):
            config["no_exist"]

    def should_return_none_if_no_exist(self):
        config = self.given_a_ini_config_service()
        assert config.get("no-exist") is None
