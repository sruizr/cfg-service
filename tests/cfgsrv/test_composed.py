from cfgsrv import ConfigService
from cfgsrv.composed import InterpolationConfigService, StackedConfigService


class StubConfigService(ConfigService):
    def __init__(self, content):
        self._content = content

    def get(self, key, default=None):
        return self._content.get(key, default)


class A_StackedConfigService:
    def should_give_any_available_key(self):
        config_1 = StubConfigService({"key_1": "value-1"})
        config_2 = StubConfigService({"key_2": "value-2"})
        config = StackedConfigService(config_1, config_2)

        assert config["key_1"] == "value-1"
        assert config["key_2"] == "value-2"

    def should_return_value_of_last_config_service(self):
        config_1 = StubConfigService({"key": "value-1"})
        config_2 = StubConfigService({"key": "value-2"})
        config = StackedConfigService(config_1, config_2)

        assert config["key"] == "value-2"

    def should_return_none_if_no_exist(self):
        config_1 = StubConfigService({"key_1": "value-1"})
        config_2 = StubConfigService({"key_2": "value-2"})
        config = StackedConfigService(config_1, config_2)

        assert config.get("no-exist") is None

    def should_merge_dictionaries_with_diferent_keys(self):
        config_1 = StubConfigService({"dict_var": {"key-1": "value-1"}})
        config_2 = StubConfigService({"dict_var": {"key-2": "value-2"}})
        config = StackedConfigService(config_1, config_2)

        assert config.get("dict_var") == {"key-1": "value-1", "key-2": "value-2"}

    def should_merge_dictionaries_keeping_config_priorities(self):
        config_1 = StubConfigService({"dict_var": {"key-1": "value-1"}})
        config_2 = StubConfigService({"dict_var": {"key-1": "value-2"}})
        config = StackedConfigService(config_1, config_2)

        assert config.get("dict_var") == {"key-1": "value-2"}

    def should_merge_subdictionaries_with_keeping_priorities(self):
        config_1 = StubConfigService({"dict_var": {"key-1": {"key": "value-1"}}})
        config_2 = StubConfigService(
            {"dict_var": {"key-1": {"key": "value-2", "other_key": "value"}}}
        )
        config = StackedConfigService(config_1, config_2)

        assert config.get("dict_var") == {
            "key-1": {"key": "value-2", "other_key": "value"}
        }


class An_InterpolationConfigService:
    def given_a_interpolation_service(self, content=None):
        if not content:
            content = {
                "key_1": "value1-${key_2}",
                "key_2": "value2",
                "dict_key": {"key_a": "${key_2}", "nested": {"key_b": "${key_2}"}},
                "list_keys": ["value", "value"],
                "key_3": "${no-exist}",
            }
        return InterpolationConfigService(StubConfigService(content))

    def should_pass_simple_valuse(self):
        config = self.given_a_interpolation_service()

        assert config["key_2"] == "value2"

    def should_interpolate_simple_pars(self):
        config = self.given_a_interpolation_service()
        assert config["key_1"] == "value1-value2"

    def should_interpolate_dictionaries(self):
        config = self.given_a_interpolation_service()

        assert config["dict_key"]["key_a"] == "value2"
        assert config["dict_key"]["nested"]["key_b"] == "value2"

    def should_return_none_if_no_exist_interpolation(self):
        config = self.given_a_interpolation_service()
        assert config.get("key_3") is None
